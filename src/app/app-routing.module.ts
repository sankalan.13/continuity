import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PostDashboardComponent} from './posts/post-dashboard/post-dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: '/blog', pathMatch: 'full' },
  { path: '', loadChildren: './posts/posts.module#PostsModule'},
  { path: 'dashboard', component: PostDashboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
