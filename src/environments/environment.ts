// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB7hD4PIqOEdZZx4-PJjT2TG0D21bUSw9I',
    authDomain: 'continuity-119cb.firebaseapp.com',
    databaseURL: 'https://continuity-119cb.firebaseio.com',
    projectId: 'continuity-119cb',
    storageBucket: 'continuity-119cb.appspot.com',
    messagingSenderId: '7653323216',
    appId: '1:7653323216:web:b18047b0b72c6f03'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
